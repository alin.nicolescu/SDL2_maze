#ifndef CELL_H
#define CELL_H

#include<stdint.h>
#include<stdbool.h>

#define UNSIGNED uint16_t

typedef struct {
    UNSIGNED row;
    UNSIGNED col;
} cell_t;

enum {EMPTY, FULL, START, END, CURRENT, VISITED, PATH};

bool good_cell(cell_t* c,UNSIGNED rows,UNSIGNED cols);
bool cells_equal(cell_t* c1,cell_t* c2);
bool north(cell_t* cell,cell_t* N);
bool west(cell_t* cell,cell_t* W);
bool south(cell_t* cell,cell_t* S,UNSIGNED rows);
bool east(cell_t* cell,cell_t* E,UNSIGNED cols);

#ifdef CELL_H_IMPLEMENTATION

bool good_cell(cell_t* c,UNSIGNED rows,UNSIGNED cols)
{
    return c->row < rows && c->col < cols;
}

bool cells_equal(cell_t* c1,cell_t* c2)
{
    return c1->row == c2->row && c1->col == c2->col;
}

bool north(cell_t* cell,cell_t* N)
{
    if(cell->row == 0) return false;
    N->row = cell->row - 1;
    N->col = cell->col;
    return true;
}

bool west(cell_t* cell,cell_t* W)
{
    if(cell->col == 0) return false;
    W->row = cell->row;
    W->col = cell->col - 1;
    return true;
}

bool south(cell_t* cell,cell_t* S,UNSIGNED rows)
{
    if(cell->row == rows-1) return false;
    S->row = cell->row + 1;
    S->col = cell->col;
    return true;
}

bool east(cell_t* cell,cell_t* E,UNSIGNED cols)
{
    if(cell->col == cols-1) return false;
    E->row = cell->row;
    E->col = cell->col + 1;
    return true;
}

#endif // CELL_H_IMPLEMENTATION

#endif // CELL_H
