#include "SDL2_template.h"
#include "SDL2_grid.h"
#include "utils.h"
#include "lists.h"

#define ROWS 20
#define COLS 20
#define GRID_OFFSET 1
#define CELL_OFFSET 2
#define DELAY 200

bool start_set;
bool end_set;
bool bfs_on;
bool dfs_on;
bool end_reached;
bool pause;

queue_t Q; // Queue for BFS
node_t* S = NULL; // Stack pointer for DFS
int cell_state[ROWS][COLS];
cell_t back[ROWS][COLS];
cell_t bad_cell = {ROWS,COLS};
cell_t start, end, current;
grid_t g;

void BFSInit(void);
void BFSNext(void);
void DFSInit(void);
void DFSNext(void);

void MazeInit(void);
void MazeReset(void);
void InitStartCell(UNSIGNED r,UNSIGNED c);
void InitEndCell(UNSIGNED r,UNSIGNED c);
void GenRandomObstacles(size_t n);
void SetPath(cell_t* p);
void ClearOldPath(void);
void DrawPath(cell_t* p,const SDL_Color* color);

int main(int argc,char* argv[])
{
    (void) argc;
    (void) argv;

    MazeInit();

    SetWidthHeigth(600,600);
    SetTitle("SDL2 Maze");
    SetBackground(&black);

    InitGraph();
    atexit(CloseGraph);

    SetOffset(&g,CELL_OFFSET);

    Loop();

    return 0;
}

void InitStartCell(UNSIGNED r,UNSIGNED c)
{
    start.row = r;
    start.col = c;
    start_set = true;
    cell_state[r][c] = START;
}

void InitEndCell(UNSIGNED r,UNSIGNED c)
{
    end.row = r;
    end.col = c;
    end_set = true;
    cell_state[r][c] = END;
}

void GenRandomObstacles(size_t n)
{
    InitSeed();
    size_t nr = 0;
    do
    {
        size_t row = RANDOM(0,ROWS-1);
        size_t col = RANDOM(0,COLS-1);
        if(cell_state[row][col] == EMPTY)
        {
            cell_state[row][col] = FULL;
            ++nr;
        }
    }
    while(nr<n);
}

void BFSInit(void)
{
    for(size_t i=0;i<ROWS;++i)
        for(size_t j=0;j<COLS;++j)
            back[i][j] = bad_cell;

   delete_queue(&Q);
   queue_push(&start,&Q);
   current = start;
   end_reached = false;
}

void DFSInit(void)
{
    for(size_t i=0;i<ROWS;++i)
        for(size_t j=0;j<COLS;++j)
            back[i][j] = bad_cell;

    delete_stack(&S);
    stack_push(&start,&S);
    current = start;
    end_reached = false;
}

void MazeInit(void)
{
    for(size_t i=0;i<ROWS;++i)
        for(size_t j=0;j<COLS;++j)
            cell_state[i][j] = EMPTY;

    InitStartCell(0,0);
    InitEndCell(ROWS-1,COLS-1);
    GenRandomObstacles(ROWS*COLS/4);
    bfs_on = false;
    dfs_on = false;
    pause = false;
    end_reached = false;
}

void MazeReset(void)
{
    for(size_t i=0;i<ROWS;++i)
        for(size_t j=0;j<COLS;++j)
        {
            if(!(cell_state[i][j] == FULL ||
                        cell_state[i][j] == EMPTY ||
                        cell_state[i][j] == START ||
                        cell_state[i][j] == END))
                cell_state[i][j] = EMPTY;
        }

    if(cell_state[start.row][start.col] != START)
        cell_state[start.row][start.col] = START;

    if(bfs_on) bfs_on = false;
    if(dfs_on) dfs_on = false;
    if(pause) pause = false;
    if(end_reached) end_reached = false;
}

void DFSNext(void)
{
    if(!(cells_equal(&current,&start) || cells_equal(&current,&end)))
        cell_state[current.row][current.col] = VISITED;

    if(is_stack_empty(S))
    {
        if(!end_reached)
            ClearOldPath();
        dfs_on = false;
        return;
    }

    if(end_reached)
    {
        SetPath(&end);
        dfs_on = false;
    }

    cell_t c;

    ClearOldPath();
    current = S->cell;
    stack_pop(&S);
    SetPath(&current);

    if(cell_state[current.row][current.col] != VISITED ||
            cells_equal(&current,&start))
    {
        if(north(&current,&c))
        {
            if(cells_equal(&c,&end))
            {
                end_reached = true;
                back[end.row][end.col] = current;
            }
            else if(cell_state[c.row][c.col] == EMPTY)
                {
                    stack_push(&c,&S);
                    back[c.row][c.col] = current;
                }
        }

        if(east(&current,&c,COLS))
        {
            if(cells_equal(&c,&end))
            {
                end_reached = true;
                back[end.row][end.col] = current;
            }
            else if(cell_state[c.row][c.col] == EMPTY)
                {
                    stack_push(&c,&S);
                    back[c.row][c.col] = current;
                }
        }

        if(south(&current,&c,ROWS))
        {
            if(cells_equal(&c,&end))
            {
                end_reached = true;
                back[end.row][end.col] = current;
            }
            else if(cell_state[c.row][c.col] == EMPTY)
                {
                    stack_push(&c,&S);
                    back[c.row][c.col] = current;
                }
        }

        if(west(&current,&c))
        {
            if(cells_equal(&c,&end))
            {
                end_reached = true;
                back[end.row][end.col] = current;
            }
            else if(cell_state[c.row][c.col] == EMPTY)
                {
                    stack_push(&c,&S);
                    back[c.row][c.col] = current;
                }
        }
    }

    if(!(cells_equal(&current,&start) || cells_equal(&current,&end)))
        cell_state[current.row][current.col] = CURRENT;
}

void BFSNext(void)
{
    if(!(cells_equal(&current,&start) || cells_equal(&current,&end)))
        cell_state[current.row][current.col] = VISITED;

    if(is_queue_empty(&Q))
    {
        if(!end_reached)
            ClearOldPath();
        bfs_on = false;
        return;
    }

    if(end_reached)
    {
        bfs_on = false;
        return;
    }

    cell_t c;
    current = Q.F->cell;
    if(!(cells_equal(&current,&start) || cells_equal(&current,&end)))
        cell_state[current.row][current.col] = CURRENT;
    queue_pop(&Q);

    if(north(&current,&c))
    {
        if(cells_equal(&c,&end))
        {
            end_reached = true;
            back[end.row][end.col] = current;
        }
        else
            if(cell_state[c.row][c.col] == EMPTY)
            {
                queue_push(&c,&Q);
                cell_state[c.row][c.col] = VISITED;
                back[c.row][c.col] = current;
            }
    }

    if(east(&current,&c,COLS) )
    {
        if(cells_equal(&c,&end))
        {
            end_reached = true;
            back[end.row][end.col] = current;
        }
        else
            if(cell_state[c.row][c.col] == EMPTY)
            {
                queue_push(&c,&Q);
                cell_state[c.row][c.col] = VISITED;
                back[c.row][c.col] = current;
            }
    }

    if(south(&current,&c,ROWS) )
    {
        if(cells_equal(&c,&end))
        {
            end_reached = true;
            back[end.row][end.col] = current;
        }
        else
            if(cell_state[c.row][c.col] == EMPTY)
            {
                queue_push(&c,&Q);
                cell_state[c.row][c.col] = VISITED;
                back[c.row][c.col] = current;
            }
    }

    if(west(&current,&c))
    {
        if(cells_equal(&c,&end))
        {
            end_reached = true;
            back[end.row][end.col] = current;
        }
        else
            if(cell_state[c.row][c.col] == EMPTY)
            {
                queue_push(&c,&Q);
                cell_state[c.row][c.col] = VISITED;
                back[c.row][c.col] = current;
            }
    }
}

void SetPath(cell_t* p)
{
    cell_t c = *p;
    while(!cells_equal(&c,&start))
    {
        if(!(cells_equal(&c,&start) || cells_equal(&c,&end)))
            cell_state[c.row][c.col] = PATH;
        c = back[c.row][c.col];
    }
}

void ClearOldPath(void)
{
    for(int i=0;i<ROWS;++i)
        for(int j=0;j<COLS;++j)
            if(cell_state[i][j] == PATH)
                cell_state[i][j] = VISITED;
}

void DrawPath(cell_t* p,const SDL_Color* color)
{
    cell_t c = *p;
    cell_t b;
    SDL_Point M1,M2;
    SDL_Color old_color;

    if(SDL_GetRenderDrawColor(renderer,&old_color.r,&old_color.g,&old_color.b,&old_color.a)<0)
    {
        fprintf(stderr,"[ERROR] Saving old drawing color in DrawPath() failed!\n");
        exit(EXIT_FAILURE);
    }
    if(SDL_SetRenderDrawColor(renderer, color->r, color->g, color->b, SDL_ALPHA_OPAQUE)<0)
    {
        fprintf(stderr,"[ERROR] Setting new drawing color in DrawPath() failed!\n");
        exit(EXIT_FAILURE);
    }

    while(!cells_equal(&c,&start))
    {
        b = back[c.row][c.col];
        /* GetCellCorner(&g,c.row,c.col,&P1); */
        /* GetCellCorner(&g,b.row,b.col,&P2); */
        /* M1.x = P1.x+g.cell_width/2; */
        /* M1.y = P1.y+g.cell_heigth/2; */
        /* M2.x = P2.x+g.cell_width/2; */
        /* M2.y = P2.y+g.cell_heigth/2; */
        GetCellMiddle(&g,c.row,c.col,&M1);
        GetCellMiddle(&g,b.row,b.col,&M2);
        SDL_RenderDrawLine(renderer,M1.x,M1.y,M2.x,M2.y);
        c = b;
    }

    if(SDL_SetRenderDrawColor(renderer,old_color.r,old_color.g,old_color.b,old_color.a)<0)
    {
        fprintf(stderr,"[ERROR] Restore old drawing color in DrawPath() failed!\n");
        exit(EXIT_FAILURE);
    }
}

void HandleKeyPress(SDL_Event e)
{
    if(e.key.keysym.sym == SDLK_b)
    {
        if(start_set && end_set && !bfs_on && !dfs_on && !end_reached)
        {
            bfs_on = true;
            BFSInit();
        }
    }
    else
    if(e.key.keysym.sym == SDLK_d)
    {
        if(start_set && end_set && !bfs_on && !dfs_on && !end_reached)
        {
            dfs_on = true;
            DFSInit();
        }
    }
    else
    if(e.key.keysym.sym == SDLK_p)
    {
        pause = !pause;
    }
    else
    if(e.key.keysym.sym == SDLK_r)
    {
            MazeReset();
    }
    else
    if(e.key.keysym.sym == SDLK_n)
    {
       MazeInit();
    }
}

void HandleMouse(SDL_Event e)
{
    if(bfs_on || dfs_on || end_reached) return;

    int x,y;
    size_t row,col;
    if(e.button.button == SDL_BUTTON_LEFT)
    {
        SDL_GetMouseState(&x,&y);
        if(GetCell(&g,x,y,&row,&col))
        {
            if(cell_state[row][col] == EMPTY)
                cell_state[row][col] = FULL;
            else
                cell_state[row][col] = EMPTY;
        }
    }
    else if(e.button.button == SDL_BUTTON_RIGHT)
    {
        SDL_GetMouseState(&x,&y);
        if(GetCell(&g,x,y,&row,&col))
        {
            if(!start_set)
            {
                cell_state[row][col] = START;
                start.row = row;
                start.col = col;
                start_set = true;
            }
            else
                if(start.row != row || start.col != col)
                {
                    if(!end_set)
                    {
                        cell_state[row][col] = END;
                        end.row = row;
                        end.col = col;
                        end_set = true;
                    }
                    else
                        if(end.row == row && end.col == col)
                        {
                            cell_state[row][col] = EMPTY;
                            end_set = false;
                        }
                        else
                        {
                            cell_state[end.row][end.col] = EMPTY;
                            cell_state[row][col] = END;
                            end.row = row;
                            end.col = col;
                            end_set = true;
                        }
                }
                else
                    if(!end_set)
                    {
                        cell_state[row][col] = EMPTY;
                        start_set = false;
                    }
        }
    }
}

void Draw()
{
    InitGrid(&g,COLS,ROWS,(width-GRID_OFFSET)/COLS,(heigth-GRID_OFFSET)/ROWS,&maroon);
    SDL_Point P={(width-GridPixelWidth(&g))/2,(heigth-GridPixelHeigth(&g))/2};
    SetCorner(&g,&P);
    DrawGrid(&g);
    for(int i=0;i<ROWS;++i)
        for(int j=0;j<COLS;++j)
            switch(cell_state[i][j])
            {
                case EMPTY:
                    FillCell(&g,i,j,&brown);
                    break;
                case FULL:
                    XCell(&g,i,j,&maroon);
                    break;
                case START:
                case END:
                    FillCell(&g,i,j,&indigo);
                    break;
                case VISITED:
                    if(dfs_on && cells_equal(&current,&start))
                        FillCell(&g,i,j,&indigo);
                    FillCell(&g,i,j,&green);
                    break;
                case CURRENT:
                    if(!end_reached)
                        FillCell(&g,i,j,&yellow);
                    break;
                case PATH:
                    FillCell(&g,i,j,&pink);
                    break;
            }
    if(dfs_on)
        DrawPath(&current,&black);
    else
        if(end_reached)
        {
            ClearOldPath();
            SetPath(&end);
            DrawPath(&end,&black);
        }
    if(!pause)
    {
        if(bfs_on)
        {
            BFSNext();
            SDL_Delay(DELAY);
        }
        else
        if(dfs_on)
        {
            DFSNext();
            SDL_Delay(DELAY);
        }
    }
}
