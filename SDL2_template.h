/*
    SDL2 template is a C single file header library for
    Simple DirectMedia Layer projects.
*/

#ifndef SDL2_TEMPLATE_H
#define SDL2_TEMPLATE_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<SDL2/SDL.h>

#define WIDTH 250
#define HEIGTH 250
#define TITLE_MAX_LENGTH 50
#define TITLE "Window title"

extern const SDL_Color        white;
extern const SDL_Color        black;
extern const SDL_Color          red;
extern const SDL_Color         blue;
extern const SDL_Color         cyan;
extern const SDL_Color       indigo;
extern const SDL_Color        green;
extern const SDL_Color       yellow;
extern const SDL_Color       orange;
extern const SDL_Color         pink;
extern const SDL_Color         gold;
extern const SDL_Color        brown;
extern const SDL_Color  saddlebrown;
extern const SDL_Color       maroon;
extern const SDL_Color       silver;

extern int width;
extern int heigth;
extern char window_title[TITLE_MAX_LENGTH];
extern SDL_Color background;

extern SDL_Window* window;
extern SDL_Renderer* renderer;

void SetWidthHeigth(int,int);
void SetTitle(const char*);
void Background(const SDL_Color* c);
void SetBackground(const SDL_Color* c);

void InitGraph();
void Loop();
void CloseGraph();

extern void Draw();
extern void HandleKeyPress(SDL_Event e);
extern void HandleMouse(SDL_Event e);

#ifdef SDL2_TEMPLATE_IMPLEMENTATION

const SDL_Color        white = {0xff,0xff,0xff,SDL_ALPHA_OPAQUE};
const SDL_Color        black = {0x00,0x00,0x00,SDL_ALPHA_OPAQUE};
const SDL_Color          red = {0xff,0x00,0x00,SDL_ALPHA_OPAQUE};
const SDL_Color         blue = {0x00,0x00,0xff,SDL_ALPHA_OPAQUE};
const SDL_Color         cyan = {0x00,0xff,0xff,SDL_ALPHA_OPAQUE};
const SDL_Color       indigo = {0x4b,0x00,0x82,SDL_ALPHA_OPAQUE};
const SDL_Color        green = {0x00,0x80,0x00,SDL_ALPHA_OPAQUE};
const SDL_Color       yellow = {0xff,0xff,0x00,SDL_ALPHA_OPAQUE};
const SDL_Color       orange = {0xff,0xa5,0x00,SDL_ALPHA_OPAQUE};
const SDL_Color         pink = {0xff,0xc0,0xcb,SDL_ALPHA_OPAQUE};
const SDL_Color         gold = {0xff,0xd7,0x00,SDL_ALPHA_OPAQUE};
const SDL_Color       brown  = {0xa5,0x2a,0x2a,SDL_ALPHA_OPAQUE};
const SDL_Color saddlebrown  = {0x8b,0x45,0x13,SDL_ALPHA_OPAQUE};
const SDL_Color       maroon = {0x80,0x00,0x00,SDL_ALPHA_OPAQUE};
const SDL_Color       silver = {0xc0,0xc0,0xc0,SDL_ALPHA_OPAQUE};

int width = WIDTH;
int heigth = HEIGTH;
char window_title[TITLE_MAX_LENGTH] = TITLE;

SDL_Color background;

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

void SetWidthHeigth(int w,int h)
{
    width=w;
    heigth=h;
}

void SetTitle(const char* title)
{
    size_t title_len = strlen(title);
    if(title_len < TITLE_MAX_LENGTH)
        strcpy(window_title,title);
    else
    {
        strncpy(window_title,title,TITLE_MAX_LENGTH-1);
        window_title[TITLE_MAX_LENGTH-1] = '\0';
    }
}

void Background(const SDL_Color* c)
{
    SDL_SetRenderDrawColor(renderer, c->r, c->g, c->b, SDL_ALPHA_OPAQUE);
}

void SetBackground(const SDL_Color* c)
{
    background = *c;
}

void InitGraph()
{
    if(SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        fprintf(stderr,"SDL_InitGraph error: %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }

    window = SDL_CreateWindow(window_title,
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            width, heigth,
            SDL_WINDOW_RESIZABLE
            );

    if(window == NULL)
    {
        fprintf(stderr,"SDL_CreateWindow error: %s\n",SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    Uint32 renderer_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;

    renderer = SDL_CreateRenderer(window, -1, renderer_flags);

    if(renderer == NULL)
    {
        fprintf(stderr,"SDL_CreateRenderer error: %s\n",SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
}

void Loop()
{
    SDL_bool done = SDL_FALSE;
    SDL_Event e;

    while(!done)
    {
        while (SDL_PollEvent(&e))
        {
            switch(e.type)
            {
                case SDL_WINDOWEVENT:
                    if(e.window.event == SDL_WINDOWEVENT_RESIZED)
                        SetWidthHeigth(e.window.data1,e.window.data2);
                    break;

                case SDL_KEYDOWN:
                    if(e.key.keysym.sym == SDLK_q)
                        done = SDL_TRUE;
                    else
                        HandleKeyPress(e);
                    break;

                case SDL_MOUSEBUTTONDOWN:
                    HandleMouse(e);
                    break;

                case SDL_QUIT:
                    done = SDL_TRUE;
                    break;
            }
        }

        Background(&background);

        SDL_RenderClear(renderer);

        Draw();

        SDL_RenderPresent(renderer);
    }
}

void CloseGraph()
{
    if(renderer) SDL_DestroyRenderer(renderer);
    if(window) SDL_DestroyWindow(window);
    SDL_Quit();
}

#endif //SDL2_TEMPLATE_IMPLEMENTATION

#endif //SDL2_TEMPLATE_H
