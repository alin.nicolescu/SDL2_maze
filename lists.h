#ifndef LISTS_H
#define LISTS_H

#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include "cell.h"

typedef struct node {
    cell_t cell;
    struct node* next;
} node_t;

typedef struct {
    node_t* F; // front pointer
    node_t* R; // rear pointer
} queue_t;

bool is_stack_empty(node_t* S);
void stack_push(cell_t* cell,node_t** S);
void stack_pop(node_t** S);
void delete_stack(node_t** S);

bool is_queue_empty(queue_t* Q);
void delete_queue(queue_t* Q);
void queue_push(cell_t* cell,queue_t* Q);
void queue_pop(queue_t* Q);

#ifdef LISTS_H_IMPLEMENTATION

bool is_stack_empty(node_t* S)
{
    return S == NULL;
}

void stack_push(cell_t* cell,node_t** S)
{
    node_t* p = (node_t*) malloc(sizeof(node_t));
    if( p==NULL )
    {
        fprintf(stderr,"[ERROR] Can't allocate new node for stack.\n");
        exit(EXIT_FAILURE);
    }
    p->cell = *cell;
    p->next = *S;
    *S = p;
}

void stack_pop(node_t** S)
{
    if(*S == NULL) return;
    node_t* p = *S;
    *S = (*S)-> next;
    free(p);
}

void delete_stack(node_t** S)
{
    while(!is_stack_empty(*S))
        stack_pop(S);
}

bool is_queue_empty(queue_t* Q)
{
    return Q->F == NULL;
}

void delete_queue(queue_t* Q)
{
    if(Q == NULL) return;
    if(is_queue_empty(Q)) return;
    node_t* p;
    while(Q->F)
    {
        p = Q->F;
        Q->F = (Q->F)->next;
        free(p);
    }
    Q->R = NULL;
}

void queue_push(cell_t* cell,queue_t* Q)
{
    node_t* p = (node_t*) malloc(sizeof(node_t));
    if( p==NULL )
    {
        fprintf(stderr,"[ERROR] Can't allocate new node for queue.\n");
        exit(EXIT_FAILURE);
    }
    p->cell = *cell;
    p->next = NULL;

    if(Q->F == NULL)
    {
        Q->F = Q->R = p;
    }
    else
    {
        (Q->R)->next = p;
        Q->R = p;
    }
}

void queue_pop(queue_t* Q)
{
    if(Q->F == NULL) return;
    node_t* p = Q->F;
    if(Q->F == Q->R)
    {
        Q->F = Q->R = NULL;
    }
    else
    {
        Q->F = (Q->F)->next;
    }
    free(p);
}

#endif // LISTS_H_IMPLEMENTATION

#endif // LISTS_H
