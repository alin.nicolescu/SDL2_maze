# SDL2 Maze

Search algorithms in a *maze*:

- [Breadth First Search](https://en.wikipedia.org/wiki/Breadth-first_search)
- [Depth First Search](https://en.wikipedia.org/wiki/Depth-first_search)

## Breadth First Search example

![Breadth First Search Example](bfs.png)

## Depth First Search example

![Depth First Search Example](dfs.png)

## Usage

- Press `q` to quit
- Use a `left` mouse click to select/deselect a cell
- Use a `right` mouse click to set/unset the *start* and *end* cell
- Press `b` to start the *Breadth First* search
- Press `d` to start the *Depth First* search
- Press `p` to toggle *pause* on/off
- Press `r` to *reset* the maze
- Press `n` to generate a *new maze*

Initial start cell is the *upper left corner* of the maze and the end cell is
the *lower left corner*.

To change the start cell, unset the end cell first.

## Build and run

Tested in:

- `Linux` (Debian and Arch)
- `FreeBSD`
- `Windows 10`

### Linux and FreeBSD

```console
make && ./maze
```

### Windows

Use the `winmake.bat` file.

The app compiles fine with [MinGW](https://sourceforge.net/projects/mingw/)
assuming you set the *path environment variable*:

```
mingw32-make.exe -f WinMakefile
```

Change the following lines from `WinMakefile`  if you installed `SDL2` in
other location:

```
INCLUDE_PATH = C:\MinGW\mingw64\include
LIBS_PATH = C:\MinGW\mingw64\libs
```

